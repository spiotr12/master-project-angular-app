export const environment = {
  production: true,
  apiUrl: 'http://localhost:5000/api',
  keywordMinChance: 10,
  keywordMaxChance: 25,
  resultTableColumns : 10
};
