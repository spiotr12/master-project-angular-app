import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ValueService } from './services/value.service';
import { IValuesResponse } from './models/values-response.interface';
import { environment } from '../environments/environment';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewChecked {

  public valuesData: IValuesResponse;
  public highlightKeyword: boolean;

  // times
  public getValuesStartTime: number;
  public getValuesEndTime: number;
  public findValuesStartTime: number;
  public findValuesEndTime: number;
  public gettingData: boolean;
  public findingData: boolean;

  ////

  private readonly numberOfRecordsSelectedByDefault: number = 10;
  public numberOfRecords: number = this.numberOfRecordsSelectedByDefault;
  public readonly numberOfRecordsOptions: number[] = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000];

  public readonly angularForNRecordsLabel: string = 'Angular for [n] records';
  public readonly renderingLabel: string = 'Rendering [ms]';
  public readonly reRenderingLabel: string = 'Re-rendering [ms]';
  public readonly placeholder: string = '---';

  public readonly columnCount: number = environment.resultTableColumns;

  public readonly runExperiment: boolean = true;
  public readonly maxIteration: number = 10;
  public readonly separator: string = ';';
  private experimentData;

  get rowCount(): number {
    return this.valuesData ? Math.ceil(this.valuesData.size / this.columnCount) : 0;
  }

  get columnIds(): number[] {
    const arr = [];

    for (let i = 0; i < this.columnCount; i++) {
      arr[i] = i;
    }

    return arr;
  }

  get rowIds(): number[] {
    const arr = [];

    for (let i = 0; i < this.rowCount; i++) {
      arr[i] = i;
    }

    return arr;
  }

  constructor(private valueService: ValueService,
              private cdRef: ChangeDetectorRef) { }

  public onNumberOfRecordsChange(value: number): void {
    this.numberOfRecords = value;
    this.reset();
  }

  public onNumberOfRecordsSet(event) {
    this.numberOfRecords = parseInt(event.target.value);
    this.reset();
  }

  public getValues() {
    this.reset();
    this.valueService.getValues(this.numberOfRecords).subscribe(
      (response: IValuesResponse) => {
        this.gettingData = true;
        this.getValuesStartTime = performance.now();
        this.valuesData = response;
      }
    );
  }

  public findValues() {
    this.highlightKeyword = true;
    this.findingData = true;
    this.findValuesStartTime = performance.now();
    this.cdRef.detectChanges();
  }

  public reset() {
    this.valuesData = null;
    this.highlightKeyword = false;
    this.getValuesStartTime = null;
    this.getValuesEndTime = null;
    this.findValuesStartTime = null;
    this.findValuesEndTime = null;
    this.gettingData = null;
    this.findingData = null;
  }

  ngOnInit(): void {
    this.initExperiment();
  }

  // Experiment
  private hasExperimentEnded(): boolean {
    return (this.experimentData.recordIndex >= this.numberOfRecordsOptions.length);
  }

  private initExperiment() {
    if (!this.runExperiment) {
      return;
    }

    let initData = {
      iterationIndex: 0, // experiment run for same records number
      recordIndex: 0, // changes data requested
      data: `frontend${this.separator}numberOfRecords${this.separator}loadingtime${this.separator}rendeting${this.separator}re-rendering`
    };

    this.experimentData = JSON.parse(sessionStorage.getItem('angularExperimentData'));
    if (!this.experimentData) {
      this.experimentData = initData;
      sessionStorage.setItem('angularExperimentData', JSON.stringify(this.experimentData));
    }

    if (this.experimentData.iterationIndex >= this.maxIteration) {
      this.experimentData.recordIndex++;
      this.experimentData.iterationIndex = 0;
    }

    if (!this.hasExperimentEnded()) {
      this.onNumberOfRecordsChange(this.numberOfRecordsOptions[this.experimentData.recordIndex]);

      // EXPERIMENT: Start
      this.experimentData.iterationIndex++;
      this.getValues();
    } else {
      console.log('Experiment has ended');
      console.log('Final experiment results:');
      console.log(this.experimentData.data);
      // this.copyStringToClipboard(this.experimentData.data);
      // console.log('Copied to clipboard');
    }
    // Remove session data
    // sessionStorage.removeItem('angularExperimentData');
  }

  private continueExperiment() {
    if (!this.runExperiment) {
      return;
    }

    // EXPERIMENT: Find results
    setTimeout(() => {
      this.findValues();
      this.cdRef.detectChanges();
    }, 1000);
  }

  private completeExperiment() {
    if (!this.runExperiment) {
      return;
    }

    // EXPERIMENT: Get all experiment data
    setTimeout(() => {
      this.experimentData.data = this.experimentData.data + this.prepareExperimentResult();
      sessionStorage.setItem('angularExperimentData', JSON.stringify(this.experimentData));
      window.location.reload(true);
    }, 1000);
  }

  private prepareExperimentResult(): string {
    let firstPaintTime = performance.timing.domComplete - performance.timing.navigationStart;
    return `\nangular${this.separator}${this.valuesData.size}${this.separator}${firstPaintTime}${this.separator}${this.getValuesTime()}${this.separator}${this.findValuesTime()}`;
  }

  // Experiment End

  ngAfterViewChecked(): void {
    // console.log('avc', performance.now())
    if (this.gettingData && this.valuesData) {
      this.getValuesEndTime = performance.now();
      this.gettingData = false;
      this.continueExperiment();
    } else if (this.findingData) {
      this.findingData = false;
      this.findValuesEndTime = performance.now();
      this.completeExperiment();
    }
    this.cdRef.detectChanges();
  }

  public getValueAt(row, col): string {
    const index = row * this.columnCount + col;
    return this.valuesData.data[index];
  }

  public getValuesTime(): any {
    if (this.getValuesEndTime && this.getValuesStartTime) {
      let result = this.getValuesEndTime - this.getValuesStartTime;
      return result.toFixed(5);
    } else {
      return this.placeholder;
    }
  }

  public findValuesTime(): any {
    if (this.findValuesEndTime && this.findValuesStartTime) {
      let result = this.findValuesEndTime - this.findValuesStartTime;
      return result.toFixed(5);
    } else {
      return this.placeholder;
    }
  }

  public copyToClipboard(): void {
    const data =
      // `${this.angularForNRecordsLabel}\t${this.renderingLabel}\t${this.reRenderingLabel}
      `${this.valuesData.size}\t${this.getValuesTime()}\t${this.findValuesTime()}`;

    this.copyStringToClipboard(data);
  }

  private copyStringToClipboard(str): void {
    // Create new element
    const el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('class', 'clipboard-data');
    el.setAttribute('readonly', '');
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
  }
}
