import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { IValuesResponse } from '../models/values-response.interface';

@Injectable({
  providedIn: 'root'
})
export class ValueService {

  constructor(private http: HttpClient) { }

  public getValues(numberOfRecords: number, minChance?: number, maxChance?: number): Observable<IValuesResponse> {
    if (!minChance) {
      minChance = Math.round(numberOfRecords * (environment.keywordMinChance / 100))
    }
    if (!maxChance) {
      maxChance = Math.round(numberOfRecords * (environment.keywordMaxChance / 100))
    }

    let url = `${environment.apiUrl}/values/${numberOfRecords}?min=${minChance}&max=${maxChance}`;

    return this.http.get<IValuesResponse>(url);
  }
}
