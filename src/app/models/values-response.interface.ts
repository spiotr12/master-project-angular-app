export interface IValuesResponse {
  data: string[],
  keyWord: string,
  keyWordCount: number,
  max: number,
  min: number,
  size: number
}
